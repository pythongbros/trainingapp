package com.example.trainingapp.ExercisesView

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.app.ActivityCompat.startActivityForResult
import androidx.recyclerview.widget.RecyclerView
import com.example.trainingapp.AppDatabase
import com.example.trainingapp.R
import kotlinx.coroutines.*

class ExerciseListAdapter (
    private val context: Context,
    private val editable: Boolean
) : RecyclerView.Adapter<ExerciseListAdapter.ListViewHolder>() {

    private var DB: AppDatabase = AppDatabase.getInstance(context)
    private var exercises: MutableList<Exercise> = mutableListOf()

    private var lastQuery: String = ""
    private var lastMuscleGroupId: Int = -1

    private var selected = -1

    // One item holder
    class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.findViewById(R.id.name)
        val info: TextView = itemView.findViewById(R.id.textViewInfo)
        val icon: ImageView = itemView.findViewById(R.id.icon)
    }

    // Setting up view for new item
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        return ListViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.exercise_item,
                parent,
                false
            )
        )
    }

    // Binding data with view
    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        val exercise = exercises[position]
        holder.name.text = exercise.name
        holder.info.text = "${muscleGroupMap[exercise.muscleGroup]} | ${difficultyMap[exercise.difficulty]}"
        holder.icon.setImageResource(muscleIconMap[exercise.muscleGroup] as Int)

        if (position == selected) {
            holder.itemView.setBackgroundColor(Color.LTGRAY)
        }
        else {
            holder.itemView.setBackgroundColor(Color.TRANSPARENT)
        }

        if (editable) {
            // On item click go to description
            holder.itemView.setOnClickListener {
                val intent = Intent(context, ExerciseDetailActivity::class.java)
                intent.putExtra("exercise", exercise)
                intent.putExtra("can_edit", editable)
                startActivityForResult(context as Activity, intent, 1, null)
            }

            // On item long click ask for delete
            holder.itemView.setOnLongClickListener {
                val builder = AlertDialog.Builder(context)
                builder.setTitle("Delete")
                builder.setMessage("Exercise \"${exercise.name}\" will be deleted.")

                builder.setPositiveButton(android.R.string.yes) { dialog, which ->

                GlobalScope.launch {

                        val db = AppDatabase.getInstance(context)
                        db.exerciseDao().delete(exercise)

                        filter(lastQuery, lastMuscleGroupId)
                    }
                }

                builder.setNegativeButton(android.R.string.no) { _, _ -> }

                builder.show()
                false
            }
        }
        else {
            // On item click select
            holder.itemView.setOnClickListener {
                val old = selected
                selected = position
                notifyItemChanged(old)
                notifyItemChanged(position)
            }

            // On item long click ask view details
            holder.itemView.setOnLongClickListener {
                val intent = Intent(context, ExerciseDetailActivity::class.java)
                intent.putExtra("exercise", exercise)
                intent.putExtra("can_edit", editable)
                startActivityForResult(context as Activity, intent, 1, null)
                false
            }
        }
    }

    // Filter collection by chosen tags (run as coroutine)
    fun filter(query: String, muscleGroupId: Int) = CoroutineScope(Dispatchers.Default).launch {

        lastQuery = query
        lastMuscleGroupId = muscleGroupId

        val fetchedExercises: MutableList<Exercise>
        var text = ""

        exercises.clear()

        // Fetch data from local DB
        if (muscleGroupId == -1) {
            fetchedExercises = DB.exerciseDao().getAll().toMutableList()
        } else {
            fetchedExercises = DB.exerciseDao().getByMuscleGroup(muscleGroupId).toMutableList()
        }

        // Set text to search by
        if(query.isNotEmpty()) {
            text = query.toLowerCase()
        }

        // Filter by search query
        for (exercise in fetchedExercises) {
            if(
                exercise.name.toLowerCase().contains(text) ||
                muscleGroupMap[exercise.muscleGroup]!!.toLowerCase().contains(text) ||
                difficultyMap[exercise.difficulty]!!.toLowerCase().contains(text)
            ) {
                exercises.add(exercise)
            }
        }

        // Access main thread to notify dataset change
        (context as Activity).runOnUiThread {
            notifyDataSetChanged()
        }
    }

    fun getSelectedPos() : Int {
        return selected
    }

    override fun getItemCount(): Int {
        return exercises.size
    }

    fun getSelectedExercise(): Exercise {
        return exercises[selected]
    }
}