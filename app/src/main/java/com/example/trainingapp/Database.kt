package com.example.trainingapp

import android.content.Context
import androidx.room.*
import com.example.trainingapp.ExercisesView.Exercise
import com.example.trainingapp.ExercisesView.ExerciseDao
import com.example.trainingapp.ScheduleView.ScheduledTraining
import com.example.trainingapp.ScheduleView.ScheduledTrainingDao
import com.example.trainingapp.TrainingsView.Round
import com.example.trainingapp.TrainingsView.Training
import com.example.trainingapp.TrainingsView.TrainingDao
import com.google.gson.Gson
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneOffset

class Converters {

    @TypeConverter
    fun listToJson(value: List<Round>?) = Gson().toJson(value)

    @TypeConverter
    fun jsonToList(value: String) = Gson().fromJson(value, Array<Round>::class.java).toList()

    @TypeConverter
    fun toDateTime(value: Long) =  LocalDateTime.ofInstant(Instant.ofEpochSecond(value), ZoneOffset.UTC)

    @TypeConverter
    fun fromDateTime(value: LocalDateTime) = value.atZone(ZoneOffset.UTC).toEpochSecond()
}


@Database(entities = [Exercise::class, Training::class, ScheduledTraining::class], version = 1)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {

    companion object {
        private var instance: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase {
            if (instance == null) {
                instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    "training-app-db"
                ).build()
            }

            return instance as AppDatabase
        }
    }
    abstract fun exerciseDao(): ExerciseDao
    abstract fun trainingDao(): TrainingDao
    abstract fun scheduledTrainingDao(): ScheduledTrainingDao
}