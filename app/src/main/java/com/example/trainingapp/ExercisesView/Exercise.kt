package com.example.trainingapp.ExercisesView

import android.os.Parcelable
import com.example.trainingapp.R
import androidx.room.*
import com.example.trainingapp.AppDatabase
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.parcelize.Parcelize


val difficultyMap = mapOf(0 to "Easy", 1 to "Medium", 2 to "Hard")
val muscleGroupMap = mapOf(0 to "Biceps", 1 to "Legs", 2 to "Abdominals", 3 to "Chest", 4 to "Back", 5 to "Buttocks", 6 to "Cardio")
val muscleIconMap = mapOf(
        0 to R.drawable.ic_bicep,
        1 to R.drawable.ic_legs,
        2 to R.drawable.ic_abdominals,
        3 to R.drawable.ic_chest,
        4 to R.drawable.ic_back,
        5 to R.drawable.ic_buttocks,
        6 to R.drawable.ic_cardio
)

@Parcelize
@Entity(tableName = "exercises")
data class Exercise(
        @PrimaryKey(autoGenerate = true)
        val id: Int,

        @ColumnInfo(name = "name") var name: String,
        @ColumnInfo(name = "muscle_group") var muscleGroup: Int,
        @ColumnInfo(name = "difficulty") var difficulty: Int,
        @ColumnInfo(name = "equipment") var equipment: String,
        @ColumnInfo(name = "description") var description: String,
        @ColumnInfo(name = "image") var image: String
) : Parcelable


// Queries
@Dao
interface ExerciseDao {
    @Query("SELECT * FROM exercises")
    fun getAll(): List<Exercise>

    @Query("SELECT name FROM exercises")
    fun getNames(): List<String>

    @Query("SELECT * FROM exercises WHERE muscle_group == :muscleGroup")
    fun getByMuscleGroup(muscleGroup: Int): List<Exercise>

    @Query("SELECT * FROM exercises WHERE id == :exercise_id")
    fun getById(exercise_id: Int) : Exercise

    @Insert
    fun insert(exercise: Exercise)

    @Update
    fun update(exercise: Exercise)

    @Delete
    fun delete(exercise: Exercise)
}

// Adding dummy samples to DB
fun generateExerciseSamples(DB: AppDatabase) {
    GlobalScope.launch {

        if (!DB.exerciseDao().getAll().isEmpty())
            return@launch

        DB.exerciseDao().insert(
                Exercise(
                        0,
                        "Squat",
                        1,
                        1,
                        "Nothing",
                        "Some squat description",
                        "sample.png"
                )
        )

        DB.exerciseDao().insert(
                Exercise(0,
                        "Push Up",
                        2,
                        2,
                        "Nothing",
                        "Some push up description",
                        "sample.png"
                )
        )

        DB.exerciseDao().insert(
                Exercise(0,
                        "Plank",
                        2,
                        0,
                        "Nothing",
                        "Some plank desciption",
                        "sample.png"
                )
        )
    }
}

