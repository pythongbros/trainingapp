package com.example.trainingapp.TrainingsView

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.trainingapp.AppDatabase
import com.example.trainingapp.R
import kotlinx.coroutines.*
import kotlin.math.round

class TrainingsListAdapter (
    private val context: Context
) : RecyclerView.Adapter<TrainingsListAdapter.ListViewHolder>() {

    private var DB: AppDatabase = AppDatabase.getInstance(context)
    private var trainings: MutableList<Training> = mutableListOf()


    // One item holder
    class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.findViewById(R.id.textViewTrainingName)
        val time: TextView = itemView.findViewById(R.id.textViewTrainingTime)
    }

    // Setting up view for new item
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        return ListViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.training_item,
                parent,
                false
            )
        )
    }

    // Binding data with view
    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        val training = trainings[position]
        holder.name.text = training.name

        var iter = 0
        var circuit_time = 0

        for (r in training.rounds) {
            if (r.workTime != -1) {
                circuit_time += r.workTime
            }
            else {
                // Estimate time for repetitions
                circuit_time += r.workReps * 5
            }

            if (iter != trainings.size - 1) {
                circuit_time += r.restTime
            }
            iter += 1
        }

        val est_time = (training.circuitsCount - 1) * (training.circuitRestTime) +
                circuit_time * training.circuitsCount

        "Estimated time: ${TrainingsUtils.secondsToFormattedStringHMS(est_time)}".also { holder.time.text = it }

        holder.itemView.setOnLongClickListener {
            val builder = AlertDialog.Builder(context)
            builder.setTitle(training.name)

            val options = arrayOf("Details", "Edit", "Delete")

            builder.setItems(options) { dialog, item ->

                when (options[item]) {
                    "Delete" -> {
                        GlobalScope.launch {

                            DB.trainingDao().delete(training)

                            (context as Activity).runOnUiThread {
                                trainings.removeAt(position)
                                notifyItemRemoved(position)
                            }
                        }
                    }

                    "Details" -> {
                        TrainingsUtils.showTrainingDescriptionDialog(training, context)
                    }

                    "Edit" -> {
                        val intent = Intent(context, BuildTrainingActivity::class.java)
                        intent.putExtra("training", training)
                        ActivityCompat.startActivityForResult(context as Activity, intent, 2, null)
                    }
                }
            }

            val dialog = builder.create()
            dialog.show()
            false
        }
    }

    // Filter collection by chosen tags (run as coroutine)
    fun filter(query: String) = CoroutineScope(Dispatchers.Default).launch {
        var text = ""

        trainings.clear()
        val fetchedTrainings: MutableList<Training> = DB.trainingDao().getAll().toMutableList()

        // Set text to search by
        if(query.isNotEmpty()) {
            text = query.toLowerCase()
        }

        // Filter by search query
        for (training in fetchedTrainings) {
            if(
                    training.name.toLowerCase().contains(text)
            ) {
                trainings.add(training)
            }
        }
        trainings.sortBy{ it.name }

        // Access main thread to notify dataset change
        (context as Activity).runOnUiThread {
            notifyDataSetChanged()
        }
    }

    override fun getItemCount(): Int {
        return trainings.size
    }

    fun insertTraining(t: Training) {
        trainings.add(t)
        notifyItemInserted(trainings.size - 1)
    }

    fun updateTraining(training: Training) {
        for (t in trainings) {
            if (t.id == training.id) {
                val pos = trainings.indexOf(t)
                trainings.set(pos, training)
                notifyItemChanged(pos)
                return
            }
        }
    }
}