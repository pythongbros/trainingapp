package com.example.trainingapp.TrainingsView

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.SearchView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.trainingapp.AppDatabase
import com.example.trainingapp.ExercisesView.*
import com.example.trainingapp.R
import com.example.trainingapp.databinding.ActivityExerciseBrowserBinding
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class ExerciseBrowserActivity : AppCompatActivity() {
    private lateinit var binding: ActivityExerciseBrowserBinding
    private lateinit var listAdapter: ExerciseListAdapter

    private var searchQuery: String = ""
    private var muscleGroupFilter: Int = -1

    private var reps : Int = 5
    private var work_time_sec : Int = 30
    private var rest_time_sec : Int = 30

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityExerciseBrowserBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Setup recycler view
        listAdapter = ExerciseListAdapter(this, false)

        binding.recyclerView.adapter = listAdapter
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        binding.recyclerView.addItemDecoration(
                DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        )

        binding.radioButtonModeTime.isChecked = true
        binding.radioGroupMode.setOnCheckedChangeListener { group, checkedId -> updateOptionsMenu() }
        updateRestTimeText()
        updateWorkTimeText()
        updateOptionsMenu()

        // Setup search view
        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextSubmit(query: String?): Boolean {
                searchQuery = query as String
                listAdapter.filter(searchQuery, muscleGroupFilter)
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                searchQuery = newText as String
                listAdapter.filter(searchQuery, muscleGroupFilter)
                return true
            }
        })

        // Setup new button
        binding.addButton.setOnClickListener {
            if (listAdapter.getSelectedPos() == -1) {
                Toast.makeText(this, "Nothing selected!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            val intent = Intent()

            val workTime =
                    if (binding.radioGroupMode.checkedRadioButtonId == R.id.radioButtonModeReps)
                        -1; else work_time_sec
            val workReps =
                    if (binding.radioGroupMode.checkedRadioButtonId == R.id.radioButtonModeReps)
                        reps; else -1

            val round = Round(listAdapter.getSelectedExercise().id, rest_time_sec, workTime, workReps)
            intent.putExtra("round", round)

            setResult(RESULT_OK, intent)
            finish()
        }

        // Setup filter button
        binding.filterButton.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Filter muscle group")

            // Choosing muscle group
            val muscleGroupTags = (listOf("All") + muscleGroupMap.values.toList()).toTypedArray()
            var checkedMuscleGroup = 0

            builder.setSingleChoiceItems(muscleGroupTags, muscleGroupFilter + 1) { dialog, which ->
                checkedMuscleGroup = which
            }

            builder.setNegativeButton("Cancel", null)
            builder.setPositiveButton("Filter") { dialog, which ->
                muscleGroupFilter = checkedMuscleGroup - 1
                listAdapter.filter(searchQuery, muscleGroupFilter)
            }

            val dialog = builder.create()
            dialog.show()
        }
    }

    private fun updateOptionsMenu() {
        val selectedOpt = binding.radioGroupMode.checkedRadioButtonId

        binding.repsCount.setText(reps.toString())
        binding.workTime.setText(TrainingsUtils.secondsToFormattedString(work_time_sec  ))

        if (selectedOpt == R.id.radioButtonModeReps) {
            binding.workTime.isEnabled = false
            binding.repsCount.isEnabled = true
        }
        else if (selectedOpt == R.id.radioButtonModeTime) {
            binding.repsCount.isEnabled = false
            binding.workTime.isEnabled = true
        }
    }

    override fun onResume() {
        super.onResume()
        listAdapter.filter(searchQuery, muscleGroupFilter)
    }

    fun setRestTime(view: View) {
        TrainingsUtils.showDurationDialog(
                "Rest Time",
                { m, s -> rest_time_sec = 60*m + s; updateRestTimeText() },
                rest_time_sec / 60,
                work_time_sec % 60,
                this
        )
    }

    fun setWorkTime(view: View) {
        TrainingsUtils.showDurationDialog(
                "Work Time",
                { m, s -> work_time_sec = 60*m + s; updateWorkTimeText() },
                work_time_sec / 60,
                work_time_sec % 60,
                this
        )
    }

    fun setRepsCount(view:View) {
        TrainingsUtils.showNumberDialog(
            "Repetitions count",
            { n -> reps = n; updateWorkRepsText() },
            reps,
            1,
            1000,
            this
        )
    }

    private fun updateWorkRepsText() {
        binding.repsCount.setText(reps.toString())
    }

    private fun updateRestTimeText() {
        binding.restTime.setText(TrainingsUtils.secondsToFormattedString(rest_time_sec))
    }

    private fun updateWorkTimeText() {
        binding.workTime.setText(TrainingsUtils.secondsToFormattedString(work_time_sec))
    }
}