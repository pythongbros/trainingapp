package com.example.trainingapp.ScheduleView

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.trainingapp.AppDatabase
import com.example.trainingapp.PrepareActivity
import com.example.trainingapp.R
import com.example.trainingapp.TrainingsView.DateItem
import com.example.trainingapp.TrainingsView.ListItem
import com.example.trainingapp.TrainingsView.ListItemEnum
import com.example.trainingapp.TrainingsView.TrainingsUtils
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.time.format.DateTimeFormatter
import java.util.*


class ScheduledTrainingListAdapter(
        private val context: Context,
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var DB: AppDatabase = AppDatabase.getInstance(context)
    // private var scheduledTrainings: MutableList<ScheduledTraining> = mutableListOf()
    private var trainingsAndDates: MutableList<ListItem> = mutableListOf()

    // One item holder
    class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.findViewById(R.id.name)
        val dateTime: TextView = itemView.findViewById(R.id.dateTime)
        val infoSection: LinearLayout = itemView.findViewById(R.id.infoSection)
        val startButton: Button = itemView.findViewById(R.id.startButton)
    }

    class DateViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val dateHeader: TextView = itemView.findViewById(R.id.dateHeader)
    }

    override fun getItemViewType(position: Int): Int {
        return trainingsAndDates[position].getType()
    }

    // Setting up view for new item
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var viewHolder: RecyclerView.ViewHolder? = null
        when (viewType) {
            ListItemEnum.TYPE_GENERAL.id -> {
                viewHolder = ListViewHolder(
                        LayoutInflater.from(parent.context).inflate(
                                R.layout.scheduled_training_item,
                                parent,
                                false
                        )
                )
            }
            ListItemEnum.TYPE_DATE.id -> {
                viewHolder = DateViewHolder(
                        LayoutInflater.from(parent.context).inflate(
                                R.layout.date_header,
                                parent,
                                false
                        )
                )
            }
        }
        return viewHolder!!
    }

    // Binding data with view
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when(holder.itemViewType) {
            ListItemEnum.TYPE_GENERAL.id -> {
                holder as ListViewHolder

                val scheduledTraining = trainingsAndDates[position] as ScheduledTraining
                val formatter = DateTimeFormatter.ofPattern("HH:mm", Locale.ENGLISH)
                val formattedDateTime = scheduledTraining.dateTime.format(formatter)

                GlobalScope.launch {
                    val training = DB.trainingDao().getById(scheduledTraining.trainingId)

                    (context as Activity).runOnUiThread {
                        holder.name.text = training.name
                        holder.dateTime.text = formattedDateTime

                        // Set on long click dialog
                        holder.infoSection.setOnLongClickListener {
                            val builder = AlertDialog.Builder(context)
                            builder.setTitle(training.name)

                            val options = arrayOf("Details", "Delete")

                            builder.setItems(options) { dialog, item ->

                                when (options[item]) {
                                    "Delete" -> deleteScheduledTraining(scheduledTraining)
                                    "Details" -> TrainingsUtils.showTrainingDescriptionDialog(
                                            training,
                                            context
                                    )
                                }
                            }

                            val dialog = builder.create()
                            dialog.show()
                            false
                        }

                        holder.startButton.setOnClickListener {
                            val intent = Intent(context, PrepareActivity::class.java)
                            intent.putExtra("training", training)
                            startActivity(context, intent, null)
                        }
                    }
                }
            }
            ListItemEnum.TYPE_DATE.id -> {
                holder as DateViewHolder

                val dateItem = trainingsAndDates[position] as DateItem
                holder.dateHeader.text = dateItem.date
            }
        }
    }

    override fun getItemCount(): Int {
        // return scheduledTrainings.size
        return trainingsAndDates.size
    }

    fun fetchData() {
        GlobalScope.launch {
            var scheduledTrainings = DB.scheduledTrainingDao().getAll().toMutableList()
            scheduledTrainings.sortedBy { it.dateTime }


            (context as Activity).runOnUiThread {
                trainingsAndDates = consolidateData(groupDataIntoHashMap(scheduledTrainings))
                notifyDataSetChanged()
            }
        }
    }

    private fun deleteScheduledTraining(scheduledTraining: ScheduledTraining) {
        GlobalScope.launch {
            DB.scheduledTrainingDao().delete(scheduledTraining)

            var scheduledTrainings = DB.scheduledTrainingDao().getAll().toMutableList()
            scheduledTrainings.sortedBy { it.dateTime }

            (context as Activity).runOnUiThread {
                Toast.makeText(context, "Scheduled training deleted", Toast.LENGTH_SHORT).show()
                trainingsAndDates = consolidateData(groupDataIntoHashMap(scheduledTrainings))
                notifyDataSetChanged()
            }
        }
    }

    private fun groupDataIntoHashMap(scheduledTrainings: MutableList<ScheduledTraining>): HashMap<String, MutableList<ScheduledTraining>> {
        // val hashMap = hashMapOf<LocalDateTime, >()
        // var hashMap: Map<String, List<String>> = mutableMapOf()
        val groupedHashMap: HashMap<String, MutableList<ScheduledTraining>> = HashMap()
        val formatter = DateTimeFormatter.ofPattern("dd MMMM yyyy", Locale.ENGLISH)

        for (training in scheduledTrainings) {
            val hashMapKey = training.dateTime.format(formatter)

            if (groupedHashMap.containsKey(hashMapKey)) {
                // The key is already in the HashMap; add the object
                // against the existing key.
                groupedHashMap[hashMapKey]!!.add(training)
            } else {
                // The key is not there in the HashMap; create a new key-value pair
                val list: MutableList<ScheduledTraining> = mutableListOf()
                list.add(training)
                groupedHashMap[hashMapKey] = list
            }
        }
        return groupedHashMap
    }

    private fun consolidateData(groupedData: HashMap<String, MutableList<ScheduledTraining>>): MutableList<ListItem> {
        val consolidatedData: MutableList<ListItem> = mutableListOf()
        for (date in groupedData.keys) {
            val dateItem = DateItem()
            dateItem.date = date
            consolidatedData.add(dateItem)

            for (training in groupedData[date]!!) {
                consolidatedData.add(training)
            }
        }
        return consolidatedData
    }
}