package com.example.trainingapp.TrainingsView

enum class ListItemEnum(val id: Int) {
    TYPE_DATE(0),
    TYPE_GENERAL(1)
}

abstract class ListItem {
//    val TYPE_DATE: Int = 0
//    val TYPE_GENERAL: Int = 1
    abstract fun getType(): Int
}