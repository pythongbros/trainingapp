package com.example.trainingapp.TrainingsView;

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.trainingapp.AppDatabase
import com.example.trainingapp.ExercisesView.Exercise
import com.example.trainingapp.ExercisesView.difficultyMap
import com.example.trainingapp.ExercisesView.muscleGroupMap
import com.example.trainingapp.ExercisesView.muscleIconMap
import com.example.trainingapp.R
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class RoundAdapter (
        private val context: Context,
        private val editable: Boolean
) : RecyclerView.Adapter<RoundAdapter.ListViewHolder>() {

private var DB: AppDatabase = AppDatabase.getInstance(context)
private var rounds: MutableList<Round> = mutableListOf()

        // Single item holder
        class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
                val name: TextView = itemView.findViewById(R.id.textViewRoundName)
                val info: TextView = itemView.findViewById(R.id.textViewRoundInfo)
                val duration: TextView = itemView.findViewById(R.id.textviewRoundDuration)
                val icon: ImageView = itemView.findViewById(R.id.roundIcon)
        }

        // Setting up view for new item
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
                return ListViewHolder(
                        LayoutInflater.from(parent.context).inflate(
                                R.layout.round_item,
                                parent,
                                false
                        )
                )
        }

        // Binding data with view
        override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
                val round = rounds[position]

                GlobalScope.launch {
                        val db = AppDatabase.getInstance(context)
                        val exercise = db.exerciseDao().getById(round.exerciseId)

                        (context as Activity).runOnUiThread {
                                holder.name.text = exercise.name
                                holder.icon.setImageResource(muscleIconMap[exercise.muscleGroup] as Int)

                                "${muscleGroupMap[exercise.muscleGroup]} | ${ difficultyMap[exercise.difficulty]}".also { holder.info.text = it }

                                val work_str: String
                                val rest_str = TrainingsUtils.secondsToFormattedString(round.restTime)

                                if (round.workReps == -1) {
                                        work_str = TrainingsUtils.secondsToFormattedString(round.workTime)
                                }
                                else {
                                        work_str = "${round.workReps} reps"
                                }

                                "work ${work_str} | rest ${rest_str}".also { holder.duration.text = it }

                                if (editable) {
                                        holder.itemView.setOnLongClickListener {
                                                val builder = AlertDialog.Builder(context)
                                                builder.setTitle(exercise.name)

                                                val options = arrayOf("Delete")

                                                builder.setItems(options) { dialog, item ->

                                                        when (options[item]) {
                                                                "Delete" -> {
                                                                        rounds.removeAt(position)
                                                                        notifyItemRemoved(position)
                                                                }
                                                        }
                                                }

                                                val dialog = builder.create()
                                                dialog.show()
                                                false
                                        }
                                }
                        }
                }
        }

        override fun getItemCount(): Int {
                return rounds.size
        }

        fun addItem(round: Round) {
                rounds.add(round)
                notifyItemInserted(rounds.size - 1)
        }

        fun getRounds(): List<Round>{
                return rounds.toList()
        }

        fun setRounds(r: List<Round>) {
                rounds.clear()
                rounds.addAll(r)
                notifyDataSetChanged()
        }
}