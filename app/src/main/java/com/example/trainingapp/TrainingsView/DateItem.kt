package com.example.trainingapp.TrainingsView

class DateItem : ListItem() {
    var date: String? = null

    override fun getType(): Int {
        return ListItemEnum.TYPE_DATE.id
    }
}