package com.example.trainingapp.ExercisesView

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.trainingapp.AppDatabase
import com.example.trainingapp.Utils.DrawerUtil
import com.example.trainingapp.databinding.ActivityExerciseBinding
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class ExerciseActivity : AppCompatActivity() {
    private lateinit var binding: ActivityExerciseBinding
    private lateinit var listAdapter: ExerciseListAdapter
    private val drawerItemIdentifier: Long = DrawerUtil.DrawerItems.EXERCISES_ITEM.identifier

    private var searchQuery: String = ""
    private var muscleGroupFilter: Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityExerciseBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // setup navigation drawer
        DrawerUtil.setupNavigationDrawer(binding.slider, drawerItemIdentifier)

        // Setup recycler view
        listAdapter = ExerciseListAdapter(this, true)

        binding.recyclerView.adapter = listAdapter
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        binding.recyclerView.addItemDecoration(
                DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        )

        // Setup search view
        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextSubmit(query: String?): Boolean {
                searchQuery = query as String
                listAdapter.filter(searchQuery, muscleGroupFilter)
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                searchQuery = newText as String
                listAdapter.filter(searchQuery, muscleGroupFilter)
                return true
            }
        })

        // Setup new button
        binding.newButton.setOnClickListener {
            val intent = Intent(this, ExerciseEditActivity::class.java)
            ActivityCompat.startActivityForResult(this, intent, 1, null)
        }

        // Setup filter button
        binding.filterButton.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Filter muscle group")

            // Choosing muscle group
            val muscleGroupTags = (listOf("All") + muscleGroupMap.values.toList()).toTypedArray()
            var checkedMuscleGroup = 0

            builder.setSingleChoiceItems(muscleGroupTags, muscleGroupFilter + 1) { dialog, which ->
                checkedMuscleGroup = which
            }

            builder.setNegativeButton("Cancel", null)
            builder.setPositiveButton("Filter") { dialog, which ->
                muscleGroupFilter = checkedMuscleGroup - 1
                listAdapter.filter(searchQuery, muscleGroupFilter)
            }

            val dialog = builder.create()
            dialog.show()
        }
    }

    override fun onResume() {
        super.onResume()
        binding.slider.selectedItemPosition = drawerItemIdentifier.toInt()
        listAdapter.filter(searchQuery, muscleGroupFilter)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(resultCode == Activity.RESULT_OK && requestCode == 1) {
            Log.d("DEBUGAPP", "Insert")

            val exercise = data?.getParcelableExtra("exercise") as Exercise?
            val context = this

            GlobalScope.launch {
                val db = AppDatabase.getInstance(context)
                db.exerciseDao().insert(exercise as Exercise)

                context.runOnUiThread {
                    listAdapter.filter(searchQuery, muscleGroupFilter)
                }
            }
        }
    }
}