package com.example.trainingapp.ExercisesView

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.core.net.toUri
import com.example.trainingapp.AppDatabase
import com.example.trainingapp.databinding.ActivityExerciseDetailBinding
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class ExerciseDetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityExerciseDetailBinding
    private var exercise: Exercise? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityExerciseDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val editable = intent.getBooleanExtra("can_edit", false)
        if (!editable) {
            binding.editButton.visibility = View.GONE
        }
    }

    override fun onResume() {
        super.onResume()

        // Retrieve item from parcelable
        val exercise = intent.getParcelableExtra("exercise") as Exercise?

        // Set view components
        binding.exerciseName.text = exercise?.name
        binding.exerciseMuscleGroup.text = muscleGroupMap[exercise?.muscleGroup]
        binding.exerciseDifficulty.text = difficultyMap[exercise?.difficulty]
        binding.exerciseEquipment.text = exercise?.equipment
        binding.exerciseDescription.text = exercise?.description
        binding.exerciseImage.setImageURI(exercise?.image?.toUri())

        // Set edit button listener
        binding.editButton.setOnClickListener {
            val intent = Intent(this, ExerciseEditActivity::class.java)
            intent.putExtra("exercise", exercise)
            ActivityCompat.startActivityForResult(this, intent, 2, null)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(resultCode == Activity.RESULT_OK && requestCode == 2) {
            Log.d("DEBUGAPP", "Update")

            val exercise = data?.getParcelableExtra("exercise") as Exercise?
            val context = this

            intent.putExtra("exercise", exercise)

            GlobalScope.launch {
                val db = AppDatabase.getInstance(context)
                db.exerciseDao().update(exercise as Exercise)
            }
        }
    }
}