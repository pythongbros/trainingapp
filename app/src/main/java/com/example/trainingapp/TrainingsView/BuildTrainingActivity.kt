package com.example.trainingapp.TrainingsView

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Gravity.CENTER
import android.view.View
import android.widget.LinearLayout
import android.widget.NumberPicker
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.view.marginLeft
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.trainingapp.AppDatabase
import com.example.trainingapp.ExercisesView.Exercise
import com.example.trainingapp.ExercisesView.ExerciseListAdapter
import com.example.trainingapp.databinding.ActivityBuildTrainingBinding
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class BuildTrainingActivity : AppCompatActivity() {
    private lateinit var binding : ActivityBuildTrainingBinding
    private lateinit var listAdapter: RoundAdapter

    private var training_id : Int = 0
    private var rest_min : Int = 0
    private var rest_sec : Int = 30
    private var round_cnt : Int = 1


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBuildTrainingBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Setup recycler view
        listAdapter = RoundAdapter(this, false)

        binding.recyclerView.adapter = listAdapter
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        binding.recyclerView.addItemDecoration(
                DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        )

        binding.imageButtonRounds.setOnClickListener { getRoundsNumberDialog() }
        binding.imageButtonRest.setOnClickListener { getRestTimeDialog() }

        binding.imageButtonAdd.setOnClickListener {
            val intent = Intent(this, ExerciseBrowserActivity::class.java)
            ActivityCompat.startActivityForResult(this, intent, 1, null)
        }

        val training = intent.getParcelableExtra("training") as Training?

        if (training != null) {
            listAdapter.setRounds(training.rounds)
            rest_min = training.circuitRestTime / 60
            rest_sec = training.circuitRestTime % 60
            round_cnt = training.circuitsCount
            binding.trainingName.setText(training.name)
            training_id = training.id
        }

        updateRestTimeText()
        updateRoundsCountText()
    }

    fun getRoundsNumberDialog() {
        val builder: android.app.AlertDialog.Builder = android.app.AlertDialog.Builder(this)
        builder.setTitle("Number of rounds")

        val np = NumberPicker(this)
        np.minValue = 1
        np.maxValue = 100
        np.value = round_cnt

        builder.setView(np)

        builder.setPositiveButton("OK", DialogInterface.OnClickListener { dialog, which ->
            round_cnt = np.value
            updateRoundsCountText()
        })
        builder.setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, which -> dialog.cancel() })

        builder.show()
    }

    private fun updateRoundsCountText() {
        binding.textViewRoundsCount.text = round_cnt.toString()
    }

    fun getRestTimeDialog() {
        TrainingsUtils.showDurationDialog(
                "Rest time",
                {m, s -> rest_min = m; rest_sec = s; updateRestTimeText() },
                rest_min,
                rest_sec,
                this
        )
    }

    private fun updateRestTimeText() {
        binding.textViewRestTime.text = TrainingsUtils.minSecToFromattedString(rest_min, rest_sec)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(resultCode == Activity.RESULT_OK && requestCode == 1) {
            Log.d("DEBUGAPP", "Inserted exercise")

            val round = data?.getParcelableExtra("round") as Round?
            if (round != null) {
                listAdapter.addItem(round)
            }
        }
    }

    fun createTraining(view: View) {
        if (binding.trainingName.text.isBlank()) {
            Toast.makeText(this, "Training name cannot be blank!", Toast.LENGTH_SHORT).show()
            return
        }

        val rounds = listAdapter.getRounds()

        if (rounds.isEmpty()) {
            Toast.makeText(this, "Cannot save empty training!", Toast.LENGTH_SHORT).show()
            return
        }

        val training = Training(
            training_id,
            binding.trainingName.text.toString(),
            rounds,
            rest_min*60 + rest_sec,
            round_cnt
        )

        val intent = Intent()
        intent.putExtra("training", training)
        setResult(RESULT_OK, intent)
        finish()
    }
}