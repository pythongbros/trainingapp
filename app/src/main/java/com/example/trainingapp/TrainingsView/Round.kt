package com.example.trainingapp.TrainingsView

import android.os.Parcelable
import com.example.trainingapp.ExercisesView.Exercise
import kotlinx.parcelize.Parcelize

@Parcelize
data class Round(
        val exerciseId : Int,
        val restTime : Int,
        val workTime : Int,
        val workReps : Int,
) : Parcelable
