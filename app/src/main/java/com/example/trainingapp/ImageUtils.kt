package com.example.trainingapp

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import java.io.File
import java.io.FileOutputStream
import java.lang.Integer.max
import kotlin.math.round

class ImageUtils {
    companion object {
        fun loadImage(pathname: String): Bitmap? {
            val imgFile = File(pathname)

            if (imgFile.exists()) {
                var bitmap = BitmapFactory.decodeFile(imgFile.absolutePath)

                if (bitmap.height > 4096 || bitmap.width > 4096) {
                    val ratio : Double = 4096.0 / max(bitmap.width, bitmap.height)
                    val width : Int = round(bitmap.width * ratio).toInt()
                    val height : Int = round(bitmap.height * ratio).toInt()

                    bitmap = Bitmap.createScaledBitmap(bitmap, width, height, true)
                }

                return bitmap
            }
            return null
        }

        fun saveImage(image: Bitmap, name: String, ctx: Context) : String {

            val dir: File = File(ctx.filesDir.absolutePath + "/images")

            if (!dir.exists()) dir.mkdirs()
            val file = File(dir, "$name.png")

            if (!file.exists()) file.createNewFile()
            val fOut = FileOutputStream(file)

            image.compress(Bitmap.CompressFormat.PNG, 85, fOut)
            fOut.flush()
            fOut.close()

            return file.absolutePath
        }
    }
}