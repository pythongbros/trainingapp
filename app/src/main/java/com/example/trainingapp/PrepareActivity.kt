package com.example.trainingapp

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import com.example.trainingapp.ExercisesView.Exercise
import com.example.trainingapp.TrainingsView.Training
import com.example.trainingapp.databinding.ActivityPrepareBinding
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class PrepareActivity : AppCompatActivity() {
    private lateinit var binding: ActivityPrepareBinding
    private var training: Training? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPrepareBinding.inflate(layoutInflater)
        setContentView(binding.root)

        training = intent.getParcelableExtra("training") as Training?

        if(training == null)
            finish()

        binding.textViewPrepareTrainingName.text = training!!.name
        "${training!!.rounds.size} exercises\n${training!!.circuitsCount} circuits".also { binding.textViewPrepareTrainingInfo.text = it }
    }

    fun startTraining(view: View) {
        val ctx = this

        GlobalScope.launch {
            val db = AppDatabase.getInstance(ctx)
            var exercises = mutableListOf<Exercise>()

            for (r in training!!.rounds) {
                exercises.add(db.exerciseDao().getById(r.exerciseId))
            }

            runOnUiThread {
                val intent = Intent(ctx, TimerActivity::class.java)
                intent.putExtra("training", training)
                intent.putParcelableArrayListExtra("exercises", ArrayList(exercises))
                ContextCompat.startActivity(ctx, intent, null)
                finish()
            }
        }

    }
}