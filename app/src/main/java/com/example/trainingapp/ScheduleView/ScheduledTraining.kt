package com.example.trainingapp.ScheduleView

import android.os.Parcelable
import androidx.room.*
import com.example.trainingapp.TrainingsView.ListItem
import com.example.trainingapp.TrainingsView.ListItemEnum
import kotlinx.parcelize.Parcelize
import java.time.LocalDateTime

@Parcelize
@Entity(tableName = "scheduled_trainings")
data class ScheduledTraining (
    @PrimaryKey(autoGenerate = true)
    val id: Int,

    @ColumnInfo(name = "training_id") val trainingId: Int,
    @ColumnInfo(name = "date_time") val dateTime: LocalDateTime

) : Parcelable, ListItem() {
    override fun getType(): Int {
        return ListItemEnum.TYPE_GENERAL.id
    }
}

// Queries
@Dao
interface ScheduledTrainingDao {
    @Query("SELECT * FROM scheduled_trainings")
    fun getAll(): List<ScheduledTraining>

    @Insert
    fun insert(scheduledTraining: ScheduledTraining)

    @Delete
    fun delete(scheduledTraining: ScheduledTraining)
}