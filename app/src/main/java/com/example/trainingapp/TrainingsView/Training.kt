package com.example.trainingapp.TrainingsView

import android.os.Parcelable
import androidx.room.*
import com.example.trainingapp.AppDatabase
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.parcelize.Parcelize


@Parcelize
@Entity(tableName = "trainings")
data class Training(
        @PrimaryKey(autoGenerate = true)
        val id: Int,
        @ColumnInfo(name = "name") val name: String,
        @ColumnInfo(name = "rounds") val rounds: List<Round>,
        @ColumnInfo(name = "circuit_rest_time") val circuitRestTime: Int,
        @ColumnInfo(name = "circuits_count") val circuitsCount: Int,
) : Parcelable


// Queries
@Dao
interface TrainingDao {
    @Query("SELECT * FROM trainings WHERE id=:id")
    fun getById(id: Int): Training

    @Query("SELECT * FROM trainings")
    fun getAll(): List<Training>

    @Insert
    fun insert(training: Training)

    @Delete
    fun delete(training: Training)

    @Update
    fun update(training: Training)
}

// Adding dummy samples to DB
fun generateTrainingSamples(DB: AppDatabase) {
    GlobalScope.launch {

        if (!DB.trainingDao().getAll().isEmpty())
            return@launch

        val rounds = arrayListOf<Round>(
                Round(
                        1,
                        60,
                        30,
                        -1,
                ),
                Round(
                        2,
                        45,
                        -1,
                        10,
                ),
                Round(
                        3,
                        30,
                        30,
                        -1,
                )
        )

        DB.trainingDao().insert(
                Training(
                        0,
                        "my training",
                        rounds,
                        60,
                        3
                )
        )
    }
}