package com.example.trainingapp.ExercisesView

import android.R
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.graphics.drawable.toBitmap
import androidx.core.net.toUri
import com.example.trainingapp.ImageUtils
import com.example.trainingapp.databinding.ActivityExerciseEditBinding


class ExerciseEditActivity : AppCompatActivity() {
    private lateinit var binding: ActivityExerciseEditBinding
    private var exercise: Exercise? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityExerciseEditBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Retrieve item from parcelable if edit else it is new
        exercise = intent.getParcelableExtra("exercise") as Exercise?

        if (exercise != null) {
            binding.exerciseName.setText((exercise as Exercise).name)
            binding.exerciseDescription.setText((exercise as Exercise).description)
            binding.exerciseEquipment.setText((exercise as Exercise).equipment)
            binding.exerciseImage.setImageURI((exercise as Exercise).image.toUri())

            val radioDifficulty = binding.radioDifficulty.getChildAt((exercise as Exercise).difficulty)
            (radioDifficulty as RadioButton).isChecked = true

            val radioMuscleGroup = binding.radioMuscleGroup.getChildAt((exercise as Exercise).muscleGroup)
            (radioMuscleGroup as RadioButton).isChecked = true
        }

        // Set image listener
        binding.exerciseImage.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Choose image")

            val options = arrayOf("Take photo", "Choose from gallery", "Cancel")

            builder.setItems(options) { dialog, item ->

                when (options[item]) {
                    "Take photo" -> {
                        val takePicture = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                        startActivityForResult(takePicture, 0)
                    }

                    "Choose from gallery" -> {
                        val pickPhoto = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                        startActivityForResult(pickPhoto, 1)
                    }

                    "Cancel" -> {
                        dialog.dismiss()
                    }
                }
            }

            val dialog = builder.create()
            dialog.show()
        }

        // Set button listener
        binding.newButton.setOnClickListener {
            // Check if all fields have been filled
            if (binding.exerciseName.text.isNullOrEmpty() ||
                binding.exerciseName.text.isBlank() ||
                binding.radioMuscleGroup.checkedRadioButtonId == -1 ||
                binding.radioDifficulty.checkedRadioButtonId == -1 ||
                binding.exerciseImage.drawable == null ) {
                Toast.makeText(this, "Please fill all required fields!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            // If null insert new item
            if(exercise == null) {
                val intent = Intent(this, ExerciseActivity::class.java)

                exercise = Exercise(
                        0,
                        binding.exerciseName.text.toString(),
                        binding.radioMuscleGroup.indexOfChild(findViewById(binding.radioMuscleGroup.checkedRadioButtonId)),
                        binding.radioDifficulty.indexOfChild(findViewById(binding.radioDifficulty.checkedRadioButtonId)),
                        binding.exerciseEquipment.text.toString(),
                        binding.exerciseDescription.text.toString(),
                        "sample.png"
                )

                intent.putExtra("exercise", exercise)
                this.setResult(Activity.RESULT_OK, intent)
                this.finish()

            // Else update existing
            } else {
                val intent = Intent(this, ExerciseDetailActivity::class.java)


                (exercise as Exercise).name = binding.exerciseName.text.toString()
                (exercise as Exercise).equipment = binding.exerciseEquipment.text.toString()
                (exercise as Exercise).description = binding.exerciseDescription.text.toString()
                (exercise as Exercise).image = ImageUtils.saveImage(binding.exerciseImage.drawable.toBitmap(), (exercise as Exercise).name, this)

                (exercise as Exercise).muscleGroup = binding.radioMuscleGroup.indexOfChild(
                    findViewById(binding.radioMuscleGroup.checkedRadioButtonId))

                (exercise as Exercise).difficulty = binding.radioDifficulty.indexOfChild(
                        findViewById(binding.radioDifficulty.checkedRadioButtonId))

                intent.putExtra("exercise", exercise)
                this.setResult(Activity.RESULT_OK, intent)
                this.finish()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

            if(resultCode == RESULT_OK && data != null) {
                when (requestCode) {
                    0 -> {
                        val selectedImage = data.getParcelableExtra("data") as Bitmap?
                        binding.exerciseImage.setImageBitmap(selectedImage)
                    }
                    1 -> {
                        binding.exerciseImage.setImageURI(data.data)
                    }
                }
            }
    }

}