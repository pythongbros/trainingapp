# TrainingApp

This is a mobile application implemented in Kotlin language.
It provides an interface to create, manage and schedule user's trainings.
The user will be able to add his own exercises with a corresponding photo
and description and finally merge them to create custom trainings.

This appliaction contains a calendar that will remind the user of
incoming trainings, that were scheduled before.

