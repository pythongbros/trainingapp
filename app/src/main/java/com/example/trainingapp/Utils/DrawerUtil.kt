package com.example.trainingapp.Utils


import android.content.Intent
import com.example.trainingapp.ExercisesView.ExerciseActivity
import com.example.trainingapp.R
import com.example.trainingapp.ScheduleView.CalendarActivity
import com.example.trainingapp.ScheduleView.ScheduleTrainingActivity
import com.example.trainingapp.TrainingsView.TrainingsActivity
import com.mikepenz.iconics.typeface.IIcon
import com.mikepenz.iconics.typeface.library.fontawesome.FontAwesome
import com.mikepenz.materialdrawer.iconics.iconicsIcon
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem
import com.mikepenz.materialdrawer.model.interfaces.nameRes
import com.mikepenz.materialdrawer.widget.MaterialDrawerSliderView


class DrawerUtil {

    enum class DrawerItems(val identifier: Long, val nameRes: Int, val iconicsIcon: IIcon) {
        CALENDAR_ITEM(0, R.string.drawer_item_calendar, FontAwesome.Icon.faw_calendar_alt),
        SCHEDULE_TRAINING_ITEM(1, R.string.drawer_item_schedule_training, FontAwesome.Icon.faw_clock),
        MY_TRAININGS_ITEM(2, R.string.drawer_item_my_trainings, FontAwesome.Icon.faw_running),
        EXERCISES_ITEM(3, R.string.drawer_item_exercises, FontAwesome.Icon.faw_dumbbell)
    }

    companion object {
        fun setupNavigationDrawer(slider: MaterialDrawerSliderView, defaultSelectedItemIdentifier: Long) {
            // create drawerItems
            val drawerItems = createDrawerItems()
            // add drawer items to the navigation drawer
            drawerItems.forEach { slider.itemAdapter.add(it) }
            // set currently selected drawer item
            slider.setSelection(defaultSelectedItemIdentifier)
            // add event listener
            slider.onDrawerItemClickListener = { v, drawerItem, _ ->
                if (defaultSelectedItemIdentifier != drawerItem.identifier) {
                    // perform below switch statement only when selected drawer item is
                    // different from currently selected
                    // start according activity (based on identifier)
                    when(drawerItem.identifier) {
                        DrawerItems.CALENDAR_ITEM.identifier -> {
                            val intent = Intent(v?.context, CalendarActivity::class.java)
                            v?.context?.startActivity(intent)
                        }
                        DrawerItems.SCHEDULE_TRAINING_ITEM.identifier -> {
                            val intent = Intent(v?.context, ScheduleTrainingActivity::class.java)
                            v?.context?.startActivity(intent)
                        }
                        DrawerItems.MY_TRAININGS_ITEM.identifier -> {
                            val intent = Intent(v?.context, TrainingsActivity::class.java)
                            v?.context?.startActivity(intent)
                        }
                        DrawerItems.EXERCISES_ITEM.identifier -> {
                            val intent = Intent(v?.context, ExerciseActivity::class.java)
                            v?.context?.startActivity(intent)
                        }
                    }
                }
                false // if we return false then drawer closes after choosing option
            }
        }

        private fun createDrawerItems(): Array<PrimaryDrawerItem> {
            return arrayOf(
                    PrimaryDrawerItem().apply {
                        nameRes = DrawerItems.CALENDAR_ITEM.nameRes;
                        identifier = DrawerItems.CALENDAR_ITEM.identifier;
                        iconicsIcon = DrawerItems.CALENDAR_ITEM.iconicsIcon;
                        isSelectable = false
                    },
                    PrimaryDrawerItem().apply {
                        nameRes = DrawerItems.SCHEDULE_TRAINING_ITEM.nameRes;
                        identifier = DrawerItems.SCHEDULE_TRAINING_ITEM.identifier;
                        iconicsIcon = DrawerItems.SCHEDULE_TRAINING_ITEM.iconicsIcon;
                        isSelectable = false
                    },
                    PrimaryDrawerItem().apply {
                        nameRes = DrawerItems.MY_TRAININGS_ITEM.nameRes;
                        identifier = DrawerItems.MY_TRAININGS_ITEM.identifier;
                        iconicsIcon = DrawerItems.MY_TRAININGS_ITEM.iconicsIcon;
                        isSelectable = false
                    },
                    PrimaryDrawerItem().apply {
                        nameRes = DrawerItems.EXERCISES_ITEM.nameRes;
                        identifier = DrawerItems.EXERCISES_ITEM.identifier;
                        iconicsIcon = DrawerItems.EXERCISES_ITEM.iconicsIcon;
                        isSelectable = false
                    }
            )
        }
    }
}