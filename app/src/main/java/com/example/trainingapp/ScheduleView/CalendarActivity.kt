package com.example.trainingapp.ScheduleView

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.trainingapp.AppDatabase
import com.example.trainingapp.ExercisesView.generateExerciseSamples
import com.example.trainingapp.TrainingsView.generateTrainingSamples
import com.example.trainingapp.Utils.DrawerUtil
import com.example.trainingapp.databinding.ActivityCalendarBinding

class CalendarActivity : AppCompatActivity() {
    private lateinit var binding: ActivityCalendarBinding
    private lateinit var listAdapter: ScheduledTrainingListAdapter
    private val drawerItemIdentifier: Long = DrawerUtil.DrawerItems.CALENDAR_ITEM.identifier

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCalendarBinding.inflate(layoutInflater)
        setContentView(binding.root)

        generateExerciseSamples(AppDatabase.getInstance(this))
        generateTrainingSamples(AppDatabase.getInstance(this))

        // Setup recycler view
        listAdapter = ScheduledTrainingListAdapter(this)

        binding.recyclerView.adapter = listAdapter
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        binding.recyclerView.addItemDecoration(
            DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        )

        supportActionBar?.title = "Calendar"
        DrawerUtil.setupNavigationDrawer(binding.slider, drawerItemIdentifier)
    }

    override fun onResume() {
        super.onResume()
        binding.slider.selectedItemPosition = drawerItemIdentifier.toInt()
        listAdapter.fetchData()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(resultCode == Activity.RESULT_OK && requestCode == 1) {
            listAdapter.notifyDataSetChanged()
        }
    }
}