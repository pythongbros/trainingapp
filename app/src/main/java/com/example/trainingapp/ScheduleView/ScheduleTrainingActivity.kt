package com.example.trainingapp.ScheduleView

import android.app.Activity
import android.app.AlertDialog
import android.app.TimePickerDialog
import android.icu.util.Calendar
import android.os.Bundle
import android.util.Log
import android.widget.CalendarView.OnDateChangeListener
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.trainingapp.AppDatabase
import com.example.trainingapp.TrainingsView.Training
import com.example.trainingapp.Utils.DrawerUtil
import com.example.trainingapp.databinding.ActivityScheduleTrainingBinding
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*


class ScheduleTrainingActivity : AppCompatActivity() {
    private lateinit var binding: ActivityScheduleTrainingBinding
    private lateinit var trainings: List<Training>
    private val drawerItemIdentifier: Long = DrawerUtil.DrawerItems.SCHEDULE_TRAINING_ITEM.identifier

    private lateinit var chosenTraining: Training
    private var dateTime: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityScheduleTrainingBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val context = this

        GlobalScope.launch {
            val db = AppDatabase.getInstance(context)
            trainings = db.trainingDao().getAll().sortedBy { it.id }

            context.runOnUiThread {
                chosenTraining = trainings[0]
                binding.trainingName.text = chosenTraining.name
            }
        }

        binding.calendarView.setOnDateChangeListener(OnDateChangeListener { view, year, month, day ->

            //show the selected date as a toast
            Toast.makeText(applicationContext, "$day/$month/$year", Toast.LENGTH_LONG).show()
            val c = Calendar.getInstance()
            c[year, month] = day
            dateTime = c.timeInMillis //this is what you want to use later
        })

        supportActionBar?.title = "Schedule training"
        DrawerUtil.setupNavigationDrawer(binding.slider, drawerItemIdentifier)

        initScheduleButton()
        initTimePicker()
        initTrainingChoice()
    }

    private fun initScheduleButton() {
        binding.scheduleButton.setOnClickListener {

            val date = Date(dateTime)
            val dateFormat = SimpleDateFormat("dd.MM.yyyy")

            val formattedDate = dateFormat.format(date)
            val formattedTime = binding.trainingTime.text

            val dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm dd.MM.yyyy")
            val dateTime = LocalDateTime.parse("$formattedTime $formattedDate", dateTimeFormatter)

            val scheduledTraining = ScheduledTraining(
                0,
                chosenTraining.id,
                dateTime
            )

            Log.d("DEBUGAPP", "Insert")

            val context = this

            GlobalScope.launch {
                val db = AppDatabase.getInstance(context)
                db.scheduledTrainingDao().insert(scheduledTraining as ScheduledTraining)

                context.runOnUiThread {
                    Toast.makeText(context, "New training scheduled", Toast.LENGTH_SHORT).show()
                }
            }
            this.setResult(Activity.RESULT_OK, intent)
            this.finish()
        }
    }

    private fun initTimePicker() {
        binding.trainingTime.setOnClickListener {
            val c: Calendar = Calendar.getInstance()
            val hh = c.get(Calendar.HOUR_OF_DAY)
            val mm = c.get(Calendar.MINUTE)

            val timePickerDialog = TimePickerDialog(
                this,
                { view, hourOfDay, minute ->

                    val hourStr = hourOfDay.toString().padStart(2, '0')
                    val minStr = minute.toString().padStart(2, '0')
                    binding.trainingTime.text = "$hourStr:$minStr"

                }, hh, mm, true
            )

            timePickerDialog.show()
        }
    }

    private fun initTrainingChoice() {

        binding.trainingName.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Choose training")

            val trainingNames = trainings.map{ it.name }.toList().toTypedArray()
            var checkedTraining = trainings.indexOf(chosenTraining)

            builder.setSingleChoiceItems(trainingNames, checkedTraining) { dialog, which ->
                checkedTraining = which
            }

            builder.setNegativeButton("Cancel", null)
            builder.setPositiveButton("Choose") { dialog, which ->
                chosenTraining = trainings[checkedTraining]
                binding.trainingName.text = chosenTraining.name
            }

            val dialog = builder.create()
            dialog.show()
        }
    }

    override fun onResume() {
        super.onResume()
        binding.slider.selectedItemPosition = drawerItemIdentifier.toInt()
    }
}