package com.example.trainingapp.TrainingsView

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.view.Gravity
import android.widget.LinearLayout
import android.widget.NumberPicker
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


data class Duration(var mins: Int, var secs: Int) {
}

class TrainingsUtils {
    companion object {
        fun secondsToFormattedString(s: Int): String {
            val mins = s / 60
            val secs = s % 60

            return mins.toString() + ":" +
            secs.toString().padStart(2, '0')
        }

        fun secondsToFormattedStringHMS(s: Int): String {
            val hours = s / 3600
            val rem_s = s - hours * 3600

            val mins = rem_s / 60
            val secs = rem_s % 60

            return hours.toString().padStart(2, '0') + ":" +
                mins.toString().padStart(2, '0') + ":" +
                    secs.toString().padStart(2, '0')
        }

        fun minSecToFromattedString(mins: Int, secs: Int): CharSequence? {
            return mins.toString() +
                    ":" +
                    secs.toString().padStart(2, '0')
        }

        fun showDurationDialog(
            title: String,
            setter: (Int, Int) -> Unit,
            default_mins: Int,
            default_secs: Int,
            ctx: Context
        )
        {
            val builder: android.app.AlertDialog.Builder = android.app.AlertDialog.Builder(ctx)
            builder.setTitle(title)

            val np_min = NumberPicker(ctx)
            val np_sec = NumberPicker(ctx)

            np_min.minValue = 0
            np_min.maxValue = 59
            np_min.value = default_mins
            np_sec.minValue = 0
            np_sec.maxValue = 59
            np_sec.value = default_secs

            val rootView = LinearLayout(ctx)
            rootView.layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            rootView.orientation = LinearLayout.HORIZONTAL
            rootView.gravity = Gravity.CENTER;

            val txt_min = TextView(ctx)
            txt_min.text = "MIN"
            val txt_sec = TextView(ctx)
            txt_sec.text = "SEC"

            rootView.addView(txt_min)
            rootView.addView(np_min)
            rootView.addView(np_sec)
            rootView.addView(txt_sec)

            builder.setView(rootView)
            builder.setPositiveButton("OK", DialogInterface.OnClickListener { dialog, which ->
                setter(np_min.value, np_sec.value)
            })
            builder.setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, which ->
                dialog.cancel()
            })

            builder.show()
        }

        fun showNumberDialog(
            title: String,
            setter: (Int) -> Unit,
            default_val: Int,
            min_val: Int,
            max_val: Int,
            ctx: Context
        ) {

            val builder: android.app.AlertDialog.Builder = android.app.AlertDialog.Builder(ctx)
            builder.setTitle(title)

            val np = NumberPicker(ctx)

            np.minValue = min_val
            np.maxValue = max_val
            np.value = default_val

            val rootView = LinearLayout(ctx)
            rootView.layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            rootView.orientation = LinearLayout.VERTICAL
            rootView.gravity = Gravity.CENTER;
            rootView.addView(np)

            builder.setView(rootView)
            builder.setPositiveButton("OK", DialogInterface.OnClickListener { dialog, which ->
                setter(np.value)
            })
            builder.setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, which ->
                dialog.cancel()
            })

            builder.show()
        }

        fun showTrainingDescriptionDialog(t: Training, ctx: Context) {
            val builder: android.app.AlertDialog.Builder = android.app.AlertDialog.Builder(ctx)
            builder.setTitle("Details of " + t.name)

            val rootView = LinearLayout(ctx)
            rootView.layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            rootView.orientation = LinearLayout.VERTICAL
            rootView.gravity = Gravity.CENTER;

            val textInfo = TextView(ctx)
            "\t\tRounds: ${t.rounds.size} | Circuits: ${t.circuitsCount} | Rest time: ${secondsToFormattedString(
                t.circuitRestTime
            )}\n\n".also { textInfo.text = it }

            val param_txt = RelativeLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            param_txt.setMargins(
                10,
                10,
                10,
                10
            ) // left, top, right, bottom

            textInfo.setLayoutParams(param_txt)

            textInfo.textSize = 18F
            rootView.addView(textInfo)

            // Setup recycler view
            val adapter = RoundAdapter(ctx, false)
            val recyclerView = RecyclerView(ctx)
            val params = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            recyclerView.layoutParams = params

            recyclerView.adapter = adapter
            recyclerView.layoutManager = LinearLayoutManager(ctx)
            recyclerView.addItemDecoration(
                DividerItemDecoration(ctx, DividerItemDecoration.VERTICAL)
            )

            for (r in t.rounds) {
                adapter.addItem(r)
            }

            rootView.addView(recyclerView)

            builder.setView(rootView)
            builder.setPositiveButton("OK", DialogInterface.OnClickListener { dialog, which ->
            })

            builder.show()
        }
    }
}