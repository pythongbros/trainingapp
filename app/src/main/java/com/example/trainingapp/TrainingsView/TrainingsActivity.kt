package com.example.trainingapp.TrainingsView

import android.app.Activity
import android.content.Intent
import com.example.trainingapp.databinding.ActivityTrainingsBinding
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.SearchView
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.trainingapp.Utils.DrawerUtil
import com.example.trainingapp.AppDatabase
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class TrainingsActivity : AppCompatActivity() {
    private lateinit var binding : ActivityTrainingsBinding
    private lateinit var listAdapter: TrainingsListAdapter
    private val drawerItemIdentifier: Long = DrawerUtil.DrawerItems.MY_TRAININGS_ITEM.identifier

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityTrainingsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        listAdapter = TrainingsListAdapter(this)
        binding.recyclerView.adapter = listAdapter
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        binding.recyclerView.addItemDecoration(
                DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        )

        // Setup search view
        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextSubmit(query: String?): Boolean {
                listAdapter.filter(query as String)
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                listAdapter.filter(newText as String)
                return true
            }
        })

        supportActionBar?.title = "My Trainings"
        DrawerUtil.setupNavigationDrawer(binding.slider, drawerItemIdentifier)
    }

    fun addTraining(view: View) {
        val intent = Intent(this, BuildTrainingActivity::class.java)
        ActivityCompat.startActivityForResult(this, intent, 1, null)
    }

    override fun onResume() {
        super.onResume()
        listAdapter.filter("")
        binding.slider.selectedItemPosition = drawerItemIdentifier.toInt()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(resultCode == Activity.RESULT_OK && requestCode == 1) {
            Log.d("DEBUGAPP", "Inserted training")

            val training = data?.getParcelableExtra("training") as Training? ?: return
            val ctx = this

            GlobalScope.launch {
                val db = AppDatabase.getInstance(ctx)
                db.trainingDao().insert(training)

                ctx.runOnUiThread {
                    listAdapter.insertTraining(training)
                }
            }
        }

        if(resultCode == Activity.RESULT_OK && requestCode == 2) {
            Log.d("DEBUGAPP", "Updated training")

            val training = data?.getParcelableExtra("training") as Training? ?: return
            val ctx = this

            GlobalScope.launch {
                val db = AppDatabase.getInstance(ctx)
                db.trainingDao().update(training)

                ctx.runOnUiThread {
                    listAdapter.updateTraining(training)
                }
            }
        }
    }
}