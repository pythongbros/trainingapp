package com.example.trainingapp

import android.app.AlertDialog
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.net.toUri
import com.example.trainingapp.ExercisesView.Exercise
import com.example.trainingapp.TrainingsView.Training
import com.example.trainingapp.TrainingsView.TrainingsUtils
import com.example.trainingapp.databinding.ActivityTimerBinding
import java.util.*

class TimerActivity : AppCompatActivity() {
    enum class Phase {
        INIT, PREPARE, WORK, REST, END
    }

    private lateinit var binding: ActivityTimerBinding
    private var training: Training? = null
    private var exercises: MutableList<Exercise> = mutableListOf()

    private var timer_sec: Int = 0;
    private var phase: Phase = Phase.INIT;
    private var exercise_indicator = 0;
    private var circuit_indicator = 0;

    private var paused: Boolean = false
    private var timer: Timer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityTimerBinding.inflate(layoutInflater)
        setContentView(binding.root)

        training = intent.getParcelableExtra("training") as Training?

        if(training == null) {
            finish()
            return
        }

        // Get all exercises
        val exercises_list = intent.getParcelableArrayListExtra<Exercise>("exercises")
        if (exercises_list == null) {
            finish()
            return
        }

        exercises = exercises_list!!.toMutableList()

        // Initial phase
        phase = Phase.INIT
        updateTimerColor()
        setClockTime(10)

        // Load exercise image if not sample.png specified
        if(exercises[exercise_indicator].image != "sample.png") {
            binding.imageViewTimerExercise.setImageURI(exercises[exercise_indicator].image.toUri())
        }

        binding.textViewTimerInfo.text = "Incoming exercise:"
        binding.textViewTimerExercise.text = exercises[exercise_indicator].name
        binding.textViewTimerPhase.text = "Prepare!"
        "Round ${exercise_indicator+1}/${training!!.rounds.size}\nCircuit ${circuit_indicator+1}/${training!!.circuitsCount}".also { binding.textViewTimerRounds.text = it }

        tickEverySecond()
    }

    fun playPause(view: View) {
        paused = !paused

        if (!paused)
            binding.imageButtonTimerPause.setImageResource(R.drawable.ic_pause)
        else
            binding.imageButtonTimerPause.setImageResource(R.drawable.ic_play)

        updateTimerColor();
    }

    private fun updateTimerColor() {
        var color_id : Int

        if (!paused) {
            if (phase == Phase.PREPARE || phase == Phase.INIT)
                color_id = R.color.custom_red
            else if (phase == Phase.WORK)
                color_id = R.color.custom_yellow
            else if (phase == Phase.REST)
                color_id = R.color.custom_green
            else
                color_id = R.color.custom_grey
        }
        else {
            color_id = R.color.custom_grey
        }

        binding.textViewTimerClock.setTextColor(getColor(color_id))
    }

    fun stop(view: View?) {
        if (timer != null) {
            timer!!.cancel();
            timer!!.purge();
        }
        finish()
    }

    private fun endTraining() {
        paused = true
        phase = Phase.END
        updateTimerColor()

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Training completed!\nPress OK to go back to the menu.")

        builder.setPositiveButton("Ok") { dialog, which ->
            stop(null)
        }
        builder.setOnDismissListener {
            stop(null)
        }

        val dialog = builder.create()
        dialog.show()

    }

    private fun tickEverySecond() {
        timer = Timer()
        timer!!.schedule(object : TimerTask() {
            override fun run() {
                runOnUiThread {
                    if (!paused) {
                        tick()
                    }
                }
            }
        }, 0, 1000) //Update text every second
    }

    private fun tick() {
        if (timer_sec > 0) timer_sec -= 1;
        binding.textViewTimerClock.text = TrainingsUtils.secondsToFormattedString(timer_sec)

        updatePhase()
    }

    private fun updatePhase() {
        if(timer_sec == 0){
            if(phase == Phase.INIT || phase == Phase.PREPARE) {
                val work_time = training!!.rounds[exercise_indicator].workTime

                // Load exercise image if not sample.png specified
                if(exercises[exercise_indicator].image != "sample.png") {
                    binding.imageViewTimerExercise.setImageURI(exercises[exercise_indicator].image.toUri())
                }

                binding.textViewTimerInfo.text = "Current exercise:"
                binding.textViewTimerExercise.text = exercises[exercise_indicator].name
                binding.textViewTimerPhase.text = "Work"
                "Round ${exercise_indicator+1}/${training!!.rounds.size}\nCircuit ${circuit_indicator+1}/${training!!.circuitsCount}".also { binding.textViewTimerRounds.text = it }

                if (work_time != -1) {
                    setClockTime(work_time)
                }
                else {
                    paused = true
                    updateTimerColor()

                    var dialog: AlertDialog? = null
                    val builder: android.app.AlertDialog.Builder = android.app.AlertDialog.Builder(this)
                    builder.setTitle("Reps")

                    val rootView = LinearLayout(this)
                    rootView.layoutParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )
                    rootView.orientation = LinearLayout.VERTICAL
                    rootView.gravity = Gravity.CENTER;

                    val textViewInfo = TextView(this)
                    textViewInfo.text = "You have ${training!!.rounds[exercise_indicator].workReps} reps to do!\nHit \"Continue\" when you are done."
                    textViewInfo.gravity = Gravity.CENTER
                    rootView.addView(textViewInfo)

                    val buttonContinue = Button(this)
                    buttonContinue.text = "Continue"
                    buttonContinue.setTextColor(getColor(R.color.custom_green))
                    buttonContinue.setOnClickListener {
                        dialog?.dismiss()
                    }
                    rootView.addView(buttonContinue)

                    builder.setView(rootView)
                    builder.setOnDismissListener {
                        paused = false
                        updateTimerColor()
                    }

                    dialog = builder.create()
                    dialog.show()
                }

                phase = Phase.WORK
                updateTimerColor()
            }
            else if(phase == Phase.WORK) {
                // If end of circuit
                if(exercise_indicator + 1 >= training!!.rounds.size) {
                    exercise_indicator = 0
                    circuit_indicator += 1
                    setClockTime(training!!.circuitRestTime)


                    if (circuit_indicator == training!!.circuitsCount) {
                        binding.textViewTimerPhase.text = "Training completed!"
                        endTraining()
                    }
                    else {
                        phase = Phase.REST
                    }
                }
                // If just another exercise
                else {
                    setClockTime(training!!.rounds[exercise_indicator].restTime)
                    exercise_indicator += 1
                    phase = Phase.REST
                }

                // Load exercise image if not sample.png specified
                if(exercises[exercise_indicator].image != "sample.png") {
                    binding.imageViewTimerExercise.setImageURI(exercises[exercise_indicator].image.toUri())
                }

                binding.textViewTimerInfo.text = "Incoming exercise:"
                binding.textViewTimerExercise.text = exercises[exercise_indicator].name
                binding.textViewTimerPhase.text = "Rest"
                "Round ${exercise_indicator+1}/${training!!.rounds.size}\nCircuit ${circuit_indicator+1}/${training!!.circuitsCount}".also { binding.textViewTimerRounds.text = it }
                updateTimerColor()
            }
            else if(phase == Phase.REST) {
                setClockTime(5)
                binding.textViewTimerPhase.text = "Prepare!"
                phase = Phase.PREPARE
                updateTimerColor()
            }
        }
    }

    fun setClockTime(seconds: Int) {
        timer_sec = seconds
        binding.textViewTimerClock.text = TrainingsUtils.secondsToFormattedString(timer_sec)
    }
}